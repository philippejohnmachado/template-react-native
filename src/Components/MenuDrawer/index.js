import React from 'react';
import { 
    View,
    Text, 
    ScrollView,
    Image,  
    Dimensions, 
    TouchableOpacity 
} from 'react-native';
import style from './style';
import IonIcons from 'react-native-vector-icons/FontAwesome';



const WIDTH =  Dimensions.get('window').width;
const HEIGTH = Dimensions.get('window').height;

export default class MenuDrawer extends React.Component{
    navLink(nav,Icon,text){
        return(
            <TouchableOpacity style={{height: 50}} onPress={() => this.props.navigation.navigate(nav)} >
            <View style={style.linemenu}> 
                <IonIcons
                    name={Icon}
                    color='#000000'
                    size={32}
                    style={style.icon}
                />
                <Text style={style.link}>{text}</Text>
            </View>
            </TouchableOpacity>
        )
    }

    render(){
        return(
            <View style={style.container}>
                <ScrollView style={style.scroller}> 
                    <View style={style.topLinks}>
                        <View style={style.profile}>
                            <View style={style.imgView}>
                                <Image style={style.img} source={ require('~/images/user.png')}/>
                            </View>
                            <View style={style.profileText}>
                                <Text style={style.name}>Nome</Text>
                            </View>
                        </View>
                    </View>
                    <View style={style.bottomLinks}>
                        {this.navLink('Home','home','Home')}
                        {this.navLink('Contato','comments','Contato')}
                        {this.navLink('Galeria','image','Galeria')}
                    </View>
                </ScrollView> 
                <View style={style.footer}>
                    <Text style={style.description}>Template</Text>
                    <Text style={style.version}>v00.01</Text>
                </View>
            </View>
        )
    } 
}
