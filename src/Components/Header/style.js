import { StyleSheet } from 'react-native';

const style = StyleSheet.create({
    header: {
        flexDirection: 'row',
        height: 60,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        backgroundColor: '#000000',
        zIndex: 10,
        justifyContent: 'center',
    },
    menuIcon:{
        zIndex: 9,
        position: 'absolute',
         top: 15,
        left: 20,
    },
    Titulo:{
        color: '#fff',
        top: 15, 
        fontSize: 30, 
        position: 'absolute',
        justifyContent: 'center'
    },
})

export default(style);