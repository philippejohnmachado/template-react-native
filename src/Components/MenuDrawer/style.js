import { StyleSheet } from 'react-native';

const style = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'lightgray',
    },
    scroller:{
       flex: 1,
    },
    profile:{
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: 25,
        borderBottomWidth: 1,
        borderBottomColor: '#777777',
    },
    profileText:{
        flex: 3,
        flexDirection: 'column',
        justifyContent: 'center', 
    },
    name:{
         fontSize: 20,
         paddingBottom: 5,
         color: 'white',
         textAlign: 'left',
    },
    imgView:{
        paddingLeft: 20,
        paddingRight: 20,
    },
    img:{
        height: 70,
        width: 70,
        borderRadius: 50,
    },  
    topLinks:{
       height: 160,
       flex: 1,
       backgroundColor: 'black',
    },
    bottomLinks:{
        flex: 1,
        backgroundColor: 'white',
        paddingTop: 10,
        paddingBottom: 450,
    },
    icon:{
       marginLeft: 10,
       marginTop: 5,
    },
    linemenu:{
        flexDirection: 'row',
    },
    link:{
        flex: 1,
        fontSize: 20,
        padding: 6,
        paddingLeft:14,
        margin:5,
        textAlign: 'left',
        color: '#000000',
    },
    footer:{
        height: 40,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        borderTopWidth: 1,
        borderTopColor: 'lightgray',
    },
    version:{
       flex: 1,
       textAlign: 'right',
       marginRight: 20,
       color: 'gray',
    },
    description:{
        flex: 1,
        textAlign: 'left',
        marginLeft: 20,
        fontSize: 16,
        color: 'gray',
    },
})

export default(style);