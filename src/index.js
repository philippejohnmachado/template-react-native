import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import DrawerNavigator from '~/Navigation/DrawerNavigator';

export default class index extends React.Component{
  render(){
    return(
      <View style={style.container}>
       <DrawerNavigator/>
      </View>
    );
  }
}

const style = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: '#fff',
  },
  });