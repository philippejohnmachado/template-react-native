import React from  'react';
import { View, Text } from 'react-native';
import IonIcons from 'react-native-vector-icons/FontAwesome';
import style from './style';

export default class Header extends React.Component{
    render(){
        return(
            <View style={style.header}>
            <IonIcons
                name='bars'
                color='#fff'
                size={32}
                style={style.menuIcon}
                onPress={() => this.props.navigation.toggleDrawer()}
            />
              <Text style={style.Titulo}>{this.props.Text}</Text>
            </View>
        )
    }
}
