import { StyleSheet } from 'react-native';

const style = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor: '#fff',
      alignItems: "center", 
      justifyContent: 'center',
    },
    text:{
      fontSize: 30,
      color: '#000000',
    },
  });

  export default(style);