import React from 'react';
import { Platform, Dimensions} from 'react-native';
import { createDrawerNavigator, createAppContainer} from 'react-navigation';
import MenuDrawer from "~/Components/MenuDrawer";
// pages
import Main from '~/pages/Main';
import Galeria from '~/pages/Galeria';
import Contato from '~/pages/Contato';


const WIDTH = Dimensions.get('window').width;

const DrawerConfig ={
    drawerWidth: WIDTH*0.83,
    contentComponent:({ navigation })=>{
        return(<MenuDrawer navigation={navigation}/>)
    }
}

const DrawerNavigator = createDrawerNavigator({
    Home: {
        screen: Main
    },
    Galeria:{
        screen: Galeria
    },
    Contato:{
        screen: Contato
    }
},
 DrawerConfig
);

export default createAppContainer(DrawerNavigator);