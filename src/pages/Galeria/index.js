import React from 'react';
import { View, Text } from 'react-native';
import Header from '~/Components/Header';
import style from './style';

export default class Galeria extends React.Component{
state = {
 text: 'Galeria'
}
  render(){
    return(
      <View style={style.container}>
      <Header  Text={this.state.text}  navigation={this.props.navigation}/>
      <Text style={style.text}>{this.state.text}</Text>
      </View>
    );
  }
}